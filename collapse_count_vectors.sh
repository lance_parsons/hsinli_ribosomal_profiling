#!/bin/bash

FILES="$1/*"
shift

echo "#"
echo -e "ID\t-20bp\t-19bp\t-18bp\t-17bp\t-16bp\t-15bp\t-14bp\t-13bp\t-12bp\t-11bp\t-10bp\t-9bp\t-8bp\t-7bp\t-6bp\t-5bp\t-4bp\t-3bp\t-2bp\t-1bp"
for f in $FILES; do
    filename=$(basename "$f")
    gene="${filename%.*}"
    echo -e -n "${gene}\t" && tr '\n' '\t' < "${f}" | sed '$s/\t$/\n/'
done
