# vi:syntax=snakemake

# Snakemake script to generate p-site offsets using plastid
# for ribosomal profiling data in ecoli

# Required inputs:
#   genome/ecoli_cds_stop_rois.txt
#   data/mapping/SAMPLE.bam

configfile: "config.yaml"

rule all:
    input: expand("data/riboprofile/{sample}/p_offsets/{sample}_stopcodon_p_offsets.txt", sample=config["samples"])

rule psite_offsets:
    input: rois="genome/ecoli_cds_stop_rois.txt",
           counts="data/mapping/{sample}.bam"
    output: "data/riboprofile/{sample}/p_offsets/{sample}_stopcodon_p_offsets.txt"
    params: base="data/riboprofile/{sample}/p_offsets/{sample}_stopcodon"
    shell: "psite {input.rois} {params.base} \
            --min_length {config[min_length]} \
            --max_length {config[max_length]} \
            --count_files {input.counts}"
