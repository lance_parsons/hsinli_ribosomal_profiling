# vi:syntax=snakemake
# Snakemake script to generate ribosomal occupancy counts using plastid

# Required inputs:
#   genome/eschericia_coli_k12_nc_000913_3.gtf
#   genome/escherichia_coli_k12_nc_000913_3_gene_annotations.gtf
#   genome/escherichia_coli_k12_nc_000913_3.bed
#   data/riboprofile/SAMPLE_p_offsets.txt
#   data/mapping/SAMPLE.bam

# Software dependencies
#   Plastid
#   samtools

import glob

configfile: "ecoli_config.yaml"
SAMPLES = config["footprint_samples"] + config["rna_samples"]
LENGTHS=range(config['min_length'], config['max_length'] + 1)
FASTA_FILE="{genome_dir}/{genome}.fasta".format(genome_dir=config["genome_dir"], genome=config["genome"])
GTF_FILE="{genome_dir}/{genome}.gtf".format(genome_dir=config["genome_dir"], genome=config["genome"])
GFF_FILE="{genome_dir}/{genome}.gff".format(genome_dir=config["genome_dir"], genome=config["genome"])
BED_FILE="{genome_dir}/{genome}.bed".format(genome_dir=config["genome_dir"], genome=config["genome"])
GENE_LIST="{genome_dir}/{genome}_gene_list.txt".format(genome_dir=config["genome_dir"], genome=config["genome"])
GENE_ANNOTATIONS="{genome_dir}/{genome}_gene_annotations.txt".format(genome_dir=config["genome_dir"], genome=config["genome"])
UPSTREAM_BED=config["genome_dir"] + "/" + config["genome"] + "_{bp}bp_upstream.bed"
UPSTREAM_BED_OVERLAP_REMOVED=config["genome_dir"] + "/" + config["genome"] + "_{bp}bp_upstream_overlap_removed.bed"
GENE_POSITIONS_OUTBASE="{genome_dir}/{genome}_plastid".format(genome_dir=config["genome_dir"], genome=config["genome"])
GENE_POSITIONS="{outbase}_gene.positions".format(outbase=GENE_POSITIONS_OUTBASE)
LANDMARK_ROI=config["genome_dir"] + "/" + config["genome"] + "_{landmark}_rois.txt"

rule all:
    input: # expand("data/riboprofile/{sample}/count_data/", sample=SAMPLES),
           expand("data/riboprofile/{sample}/{sample}_riboprofile_fw.wig", sample=SAMPLES),
           expand("data/riboprofile/{sample}/{sample}_riboprofile_rc.wig", sample=SAMPLES),

           expand("data/wiggle/{sample}_threeprime_0_profile_fw.wig", sample=config['rna_samples']),
           expand("data/wiggle/{sample}_threeprime_0_profile_rc.wig", sample=config['rna_samples']),
           expand("data/wiggle/{sample}_fiveprime_0_profile_fw.wig", sample=config['rna_samples']),
           expand("data/wiggle/{sample}_fiveprime_0_profile_rc.wig", sample=config['rna_samples']),
           expand("data/wiggle/{sample}_center_0_profile_fw.wig", sample=config['rna_samples']),
           expand("data/wiggle/{sample}_center_0_profile_rc.wig", sample=config['rna_samples']),

           # expand("data/gene_counts/{sample}_simple_counts_in_genes_annotated.txt", sample=SAMPLES),
           # expand("data/gene_counts/{sample}_cs_counts_annotated.txt", sample=SAMPLES),
           expand("data/gene_counts/{sample}_custom_counts_annotated.txt", sample=SAMPLES),
           # "data/gene_counts/charts/cs_chart_bigtable.txt"
           expand("data/codon_counts/{sample}_codon_counts.txt", sample=SAMPLES),
           expand("data/codon_counts/{sample}_threeprime_0_codon_counts.txt", sample=config['rna_samples']),
           expand("data/codon_counts/{sample}_fiveprime_0_codon_counts.txt", sample=config['rna_samples']),
           expand("data/codon_counts/{sample}_center_0_codon_counts.txt", sample=config['rna_samples']),
           expand("data/riboprofile/{sample}/{landmark}_metagene_overview.png", sample=SAMPLES, landmark=config["landmarks"]),
           expand("data/upstream_count_vectors/{sample}_{upstream_bp}bp_upstream_count_table_annotated.txt", sample=SAMPLES, upstream_bp=config["upstream_bp"]),
           #expand("data/count_vectors_genes_by_length/{sample}", sample=SAMPLES),
           #expand("data/count_vectors_upstream{upstream_bp}_by_length/{sample}", sample=SAMPLES, upstream_bp=config["upstream_bp"])


rule index_bam:
    input: "{file}.bam"
    output: "{file}.bai"
    shell: 'samtools index "{input}" "{output}"'

rule bgzip_gtf:
    input: "{file}.gtf"
    output: "{file}.gtf.gz"
    shell: 'sort -k1,1 -k4,4n "{input}" | bgzip > "{output}"'

rule tabix:
    input: "{file}.gz"
    output: "{file}.gz.tbi"
    shell: 'tabix -p gff "{input}"'

rule gene_list:
    input: BED_FILE
    output: GENE_LIST
    shell: 'awk \'{{print $4}}\' "{input}" > "{output}"'

rule upstream_rois:
    input: GTF_FILE
    output: UPSTREAM_BED
    shell: 'python simulate_utr.py --upstream_utr_length {wildcards.bp} \
            --output_utr_as_bed "{input}" > "{output}"'

rule upstream_rois_overlap_removed:
    input: upstream=UPSTREAM_BED,
           genes=GTF_FILE
    output: UPSTREAM_BED_OVERLAP_REMOVED
    shell: 'bedtools subtract -a "{input.upstream}" -b "{input.genes}" > "{output}"'

rule count_vectors:
    input: gtf=GTF_FILE + ".gz",
           tbi=GTF_FILE + ".gz.tbi",
           counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai"
    output: "data/riboprofile/{sample}/count_data"
    shell: 'get_count_vectors \
           --sorted --annotation_files "{input.gtf}" \
           --add_three \
           --count_files "{input.counts}" \
           --threeprime \
           --offset {config[offset]}\
           "{output}" && \
           touch "{output}"'

rule wiggle:
    input: counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai",
    output: fw="data/riboprofile/{sample}/{sample}_riboprofile_fw.wig",
            rc="data/riboprofile/{sample}/{sample}_riboprofile_rc.wig"
    params: base="data/riboprofile/{sample}/{sample}_riboprofile"
    shell: 'make_wiggle -o "{params.base}" \
            --count_files "{input.counts}" \
            --min_length {config[min_length]} \
            --max_length {config[max_length]} \
            --threeprime \
            --offset {config[offset]}'

rule wiggle_specify_mapping:
    input: counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai",
    output: fw="data/wiggle/{sample}_{mapping_function}_{offset}_profile_fw.wig",
            rc="data/wiggle/{sample}_{mapping_function}_{offset}_profile_rc.wig",
    params: base="data/wiggle/{sample}_{mapping_function}_{offset}_profile"
    shell: 'make_wiggle -o "{params.base}" \
            --count_files "{input.counts}" \
            --min_length {config[min_length]} \
            --max_length {config[max_length]} \
            --{wildcards.mapping_function} \
            --offset {wildcards.offset} \
            --nibble {wildcards.offset}'

rule counts_in_genes:
    input: gtf=GTF_FILE + ".gz",
           tbi=GTF_FILE + ".gz.tbi",
           counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai",
           offsets="data/riboprofile/{sample}/p_offsets/{sample}_p_offsets.txt"
    output: temp("data/gene_counts/{sample}_simple_counts_in_genes.txt")
    shell: 'counts_in_region "{output}" \
            --sorted --annotation_files "{input.gtf}" \
            --add_three \
            --count_files "{input.counts}" \
            --min_length {config[min_length]} \
            --max_length {config[max_length]} \
            --threeprime \
            --offset {config[offset]}'

rule cs_generate:
    input: gtf=GTF_FILE + ".gz",
           tbi=GTF_FILE + ".gz.tbi"
    output: GENE_POSITIONS
    params: GENE_POSITIONS_OUTBASE
    shell: 'cs generate "{params.outbase}" --sorted --annotation_files "{input.gtf}" --add_three'

rule cs_count:
    input: counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai",
           gene_positions=GENE_POSITIONS
    output: "data/gene_counts/{sample}_cs_counts.txt"
    log: "data/gene_counts/{sample}_cs_counts.log"
    params: outbase="data/gene_counts/{sample}_cs_counts"
    shell: 'cs count \
            --count_files "{input.counts}" \
            --min_length {config[min_length]} \
            --max_length {config[max_length]} \
            --threeprime \
            --offset {config[offset]} \
            "{input.gene_positions}" \
            "{params.outbase}" 2> "{log}"'

rule cs_chart:
    input: count_files=expand("data/gene_counts/{sample}_cs_counts.txt", sample=SAMPLES),
           genelist=GENE_LIST
    output: "data/gene_counts/charts/cs_chart_bigtable.txt",
            "data/gene_counts/charts/cs_chart_bintable.txt"
    params: outbase= "data/gene_counts/charts/cs_chart"
    shell: 'cs chart "{input.genelist}" "{params.outbase}" -i {input.count_files}'

# TODO Convert to using BED?
rule custom_counts:
    input: gtf=GTF_FILE,
           counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai"
    output: temp("data/gene_counts/{sample}_custom_counts.txt")
    shell: 'python transcript_counts.py \
            --codon_buffer={config[codon_buffer]} \
            --min_length={config[min_length]} \
            --max_length={config[max_length]} \
            --gtf="{input.gtf}" \
            --add_three \
            --bam="{input.counts}" \
            --offset {config[offset]} \
            > {output}'

rule annotate_gene_counts:
    input: annotation=GENE_ANNOTATIONS,
           counts="data/{dir}/{sample}_{count_type}.txt"
    output: "data/{dir}/{sample}_{count_type}_annotated.txt"
    shell: 'grep "^#" "{input.counts}" > "{output}"; \
            grep -v "^#" "{input.counts}" | \
            csvjoin - "{input.annotation}" --tabs -c1 | \
            csvformat -T >> {output}'

rule codon_counts:
    input: gff=GFF_FILE,
           fasta=FASTA_FILE,
           counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai"
    output: "data/codon_counts/{sample}_codon_counts.txt"
    log: "data/codon_counts/{sample}_codon_count.log"
    shell: 'python codon_usage.py \
            --bam "{input.counts}" \
            --gff "{input.gff}" \
            --fasta "{input.fasta}" \
            --min_length={config[min_length]} \
            --max_length={config[max_length]} \
            --add_three \
            --offset {config[offset]} \
            -o "{output}" 2> "{log}"'

rule codon_counts_specify_offset:
    input: gff=GFF_FILE,
           fasta=FASTA_FILE,
           counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai"
    output: "data/codon_counts/{sample}_{mapping_function}_{offset}_codon_counts.txt"
    log: "data/codon_counts/{sample}_codon_count.log"
    shell: 'python codon_usage.py \
            --bam "{input.counts}" \
            --gff "{input.gff}" \
            --fasta "{input.fasta}" \
            --min_length={config[min_length]} \
            --max_length={config[max_length]} \
            --add_three \
            --mapping_function {wildcards.mapping_function} \
            --offset {wildcards.offset} \
            --nibble {wildcards.offset} \
            -o "{output}" 2> "{log}"'



rule metagene_counts:
    input: roi=LANDMARK_ROI,
           counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai"
    output: profile="data/riboprofile/{sample}/{landmark}_metagene_profile.txt",
            overview="data/riboprofile/{sample}/{landmark}_metagene_overview.png"
            #rawcounts="data/riboprofile/{sample}/{landmark}_rawcounts.txt.gz",
            #normcounts="data/riboprofile/{sample}/{landmark}_normcounts.txt.gz",
            #mask="data/riboprofile/{sample}/{landmark}_mask.txt.gz"
    log: "data/riboprofile/{sample}/metagene_count.log"
    shell: 'metagene count {input.roi} \
            "data/riboprofile/{wildcards.sample}/{wildcards.landmark}" \
            --count_files "{input.counts}" \
            --threeprime 2> "{log}"'

# rule remove_intersecting_genes:

rule upstream_count_vectors:
    input: roi=UPSTREAM_BED_OVERLAP_REMOVED,
           counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai"
    output: temp("data/upstream_count_vectors/{sample}_{bp}bp_tmp")
    shell: 'get_count_vectors --count_files "{input.counts}" \
            --annotation_files "{input.roi}" \
            --annotation_format BED \
            --min_length {config[min_length]} \
            --max_length {config[max_length]} \
            --threeprime \
            --offset {config[offset]} \
            "{output}"'

rule upstream_count_table:
    input: "data/upstream_count_vectors/{sample}_{bp}bp_tmp"
    output: temp("data/upstream_count_vectors/{sample}_{bp}bp_upstream_count_table.txt")
    shell: './collapse_count_vectors.py "{input}" --length={wildcards.bp} > "{output}"'

rule test:
    #input: expand("data/count_vectors_by_length/{sample}_{bp}bp_tmp", sample=SAMPLES, bp=LENGTHS)
    #input: expand("data/count_vectors_by_length/Cup_tmp/{bp}", bp=LENGTHS)
    input: "data/count_vectors_genes_by_length/Cup",
           "data/count_vectors_upstream20_by_length/Cup"

rule count_vectors_genes_by_length:
    input: gtf=GTF_FILE + ".gz",
           tbi=GTF_FILE + ".gz.tbi",
           counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai"
    output: temp("data/count_vectors_genes_by_length_tmp/{sample}/{bp}")
    shell: 'get_count_vectors --count_files "{input.counts}" \
            --annotation_files "{input.gtf}" \
            --add_three \
            --annotation_format GTF2 \
            --min_length {wildcards.bp} \
            --max_length {wildcards.bp} \
            --threeprime \
            --offset {config[offset]} \
            "{output}"'

rule join_count_vectors_genes:
    input: lambda wildcards: expand("data/count_vectors_genes_by_length_tmp/" + wildcards.sample + "/{bp}", bp=LENGTHS)
    output: "data/count_vectors_genes_by_length/{sample}"
    script: "combine_count_vectors.py"


rule count_vectors_upstream_by_length:
    input: roi=UPSTREAM_BED_OVERLAP_REMOVED,
           counts="data/mapping/{sample}.bam",
           counts_bai="data/mapping/{sample}.bai"
    output: temp("data/count_vectors_upstream{upstreambp}_by_length_tmp/{sample}/{bp}")
    shell: 'get_count_vectors --count_files "{input.counts}" \
            --annotation_files "{input.roi}" \
            --annotation_format BED \
            --min_length {wildcards.bp} \
            --max_length {wildcards.bp} \
            --threeprime \
            --offset {config[offset]} \
            "{output}"'

rule join_count_vectors_upstream:
    input: lambda wildcards: expand("data/count_vectors_upstream" + wildcards.upstreambp + "_by_length_tmp/"+ wildcards.sample + "/{bp}", bp=LENGTHS)
    output: "data/count_vectors_upstream{upstreambp}_by_length/{sample}"
    script: "combine_count_vectors.py"
