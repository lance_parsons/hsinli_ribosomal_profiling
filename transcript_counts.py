#!/usr/bin/env python
""" Count ribosome occupancy for each transcript from riboprofile data.
"""

from __future__ import division

import argparse
import datetime
import logging
# import numpy
from plastid import GTF2_Reader
from plastid import BAMGenomeArray
from plastid import ThreePrimeMapFactory
from plastid import SizeFilterFactory

__version__ = "0.1"
__author__ = "Lance Parsons"
__author_email__ = "lparsons@princeton.edu"
__copyright__ = "Copyright 2016, Lance Parsons, Princeton University"
__license__ = \
    "BSD 2-Clause License http://www.opensource.org/licenses/BSD-2-Clause"


def main(args, loglevel):

    logging.basicConfig(format="%(levelname)s: %(message)s", level=loglevel)

    # start codons are hyper-phased; stop codons can have differnt
    # phasing or even be de-phased depending on experimental protocol
    # so, we'll ignore 5 codons after the start, and 5 before the stop
    codon_buffer = args.codon_buffer*3

    transcripts = list(GTF2_Reader(open(args.gtf),
                                   add_three_for_stop=args.add_three))
    alignments = BAMGenomeArray([args.bam])
    alignments.set_mapping(ThreePrimeMapFactory(offset=args.offset))
    alignments.add_filter("size", SizeFilterFactory(min=args.min_length,
                                                    max=args.max_length))
    dataset_counts = 0
    transcript_count_dict = dict()
    fields = ("region_name", "region", "counts", "counts_per_nucleotide",
              "rpkm", "length")
    for (i, transcript) in enumerate(transcripts):
        if(i == 0 or (i + 1) % 1000 == 0):
            logging.info("Evaluated %s genes\n" % (i + 1))
        logging.debug("%s: %s" % (transcript.get_gene(), transcript))
        if len(transcript) > 0:
            counts = transcript.get_counts(
                alignments)[codon_buffer:-codon_buffer]
            # logging.debug("Counts: %s" % counts)
            transcript_counts = int(sum(counts))
            dataset_counts += transcript_counts
            transcript_count_dict[transcript.get_gene()] = {
                "region_name": transcript.get_gene(),
                "region": transcript,
                "counts": int(transcript_counts),
                "counts_per_nucleotide":
                transcript_counts/transcript.get_length(),
                "rpkm": None,
                "length": transcript.get_length()}
    # Print output
    print("## date = %s" % datetime.date.today())
    print("## total_dataset_counts: %s" % dataset_counts)
    print("\t".join(fields))
    scaling_factor = 1000 * 1e6 / dataset_counts
    for transcript in transcripts:
        record = transcript_count_dict[transcript.get_gene()]
        record["rpkm"] = record["counts"] / record["length"] * scaling_factor
        print("\t".join(str(record.get(k)) for k in fields))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Calculates codon usage for Ribo-seq data",
        epilog="As an alternative to the commandline, params can be placed in "
        "a file, one per line, and specified on the commandline like "
        "'%(prog)s @params.conf'.",
        fromfile_prefix_chars='@')
    parser.add_argument("--bam", help="BAM file of mapped reads",
                        metavar="FILE", required=True)
    parser.add_argument("--gtf", help="GTF file of gene annotations",
                        metavar="FILE", required=True)
    parser.add_argument("--add_three", default=False, action="store_true",
                        help="If supplied, coding regions will be extended by "
                        "3 nucleotides at their 3\' ends (except for GTF2 "
                        "files that explicitly include `stop_codon` "
                        "features). Use if your annotation file excludes stop "
                        "codons from CDS.")
    parser.add_argument("--codon_buffer", help="Number of codons to ignore "
                        "after the start and before the stop (default: "
                        "%(default)s)", type=int, default=4, metavar="N")
    parser.add_argument("--offset", help="Integer representing the offset "
                        "into the read, starting from the 3' end, at which "
                        "data should be plotted (default: %(default)s)",
                        type=int, default=0, metavar="OFFSET")
    parser.add_argument("--min_length", help="Minimum length of reads to "
                        "count (default: %(default)s)", type=int, default=29,
                        metavar="N")
    parser.add_argument("--max_length", help="Minimum length of reads to "
                        "count (default: %(default)s)", type=int, default=35,
                        metavar="N")
    parser.add_argument("-v", "--verbose",
                        help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--version", action="version",
                        version="%(prog)s " + __version__)
    args = parser.parse_args()

    # Setup logging
    if args.verbose:
        loglevel = logging.DEBUG
    else:
        loglevel = logging.INFO

    main(args, loglevel)
