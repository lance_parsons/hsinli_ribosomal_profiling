#!/usr/bin/env python
""" TODO Doctring
"""

import argparse
import logging
import os

__version__ = "0.1"
__author__ = "Lance Parsons"
__author_email__ = "lparsons@princeton.edu"
__copyright__ = "Copyright 2016, Lance Parsons, Princeton University"
__license__ = \
    "BSD 2-Clause License http://www.opensource.org/licenses/BSD-2-Clause"


def main(args, loglevel):

    logging.basicConfig(format="%(levelname)s: %(message)s", level=loglevel)

    files = os.listdir(args.directory)
    headerlist = ["-{}bp".format(n) for n in range(1, args.length + 1)]
    print("#")
    print("ID\t{}".format("\t".join(headerlist)))
    for f in files:
        if os.path.isfile(os.path.join(args.directory, f)):
            logging.debug(f)
            gene = os.path.splitext(f)[0]
            with open(os.path.join(args.directory, f)) as fh:
                lines = fh.readlines()
            values = ["NaN"] * args.length
            for i, line in enumerate(lines):
                logging.debug("{}: {}".format(i, line.strip()))
                if i < args.length:
                    values[i] = int(float(line.strip()))
            print("{}\t{}".format(gene, "\t".join(format(n) for n in values)))

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Collapse count vector output into a table, padding with "
                    "NaN",
        epilog="As an alternative to the commandline, params can be placed in "
        "a file, one per line, and specified on the commandline like "
        "'%(prog)s @params.conf'.",
        fromfile_prefix_chars='@')
    # TODO Specify your real parameters here.
    parser.add_argument("directory",
                        help="Directory containing count vector files, one "
                             "per gene",
                        metavar="DIR")
    parser.add_argument("--length", type=int,
                        help="Length to trim/pad each vector to (width of "
                             "final table)",
                        metavar="INT")
    parser.add_argument("-v", "--verbose",
                        help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--version", action="version",
                        version="%(prog)s " + __version__)
    args = parser.parse_args()

    # Setup logging
    if args.verbose:
        loglevel = logging.DEBUG
    else:
        loglevel = logging.INFO

    main(args, loglevel)
