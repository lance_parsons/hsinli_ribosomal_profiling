import glob
import subprocess
import os

def do_something(data_paths, out_path, min_length, max_length):
    if not os.path.exists(out_path):
            os.mkdir(out_path)
    data_path = os.path.dirname(data_paths[0])
    lengths = range(min_length, max_length + 1)
    genes = glob.glob(os.path.join(data_paths[0], "*"))
    for genepath in genes:
        gene = os.path.basename(genepath)
        output_fh = open(os.path.join(out_path, gene), 'w')
        gene_files = []
        for length in lengths:
            gene_files.append("{}/{}/{}".format(data_path, length, gene))
        subprocess.call(["paste"] + gene_files, stdout=output_fh) 
        output_fh.close()

do_something(snakemake.input, snakemake.output[0], snakemake.config["min_length"], snakemake.config["max_length"])
