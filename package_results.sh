#!/bin/bash

REV=$(git rev-parse --short HEAD)
NOW=$(date +"%Y-%m-%d")
OUTPUT_FILENAME="hsinli_ribosomal_profiling_results_${NOW}_${REV}.tar.gz"
tar cvf -  \
    --exclude='data/mapping' \
    --exclude='data/*_tmp' \
    --exclude='data/*.old' \
    --exclude='*.DS_Store' \
    "data/" \
    "README.md" \
    *config.yaml \
    "counts.snakefile" \
    "offsets.snakefile" | \
    pigz -9 - > "${OUTPUT_FILENAME}" 
